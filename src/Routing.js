import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomePage from './HomePage';
import NetinfoPage from './NetinfoPage';
// import DateTimePickerPage from './DateTimePickerPage';
import SliderPage from './SliderPage';
import GeolocationPage from './GeolocationPage';
// // import ProgressviewPage from './src/ProgressviewPage';
// // import ProgressBarAndroidPage from './src/ProgressBarAndroidPage';
import ClipboardPage from './ClipboardPage';
import AsyncStoragePage from './AsyncStoragePage';
// // import CarouselPage from './src/CarouselPage';
import ImageZoomViewerPage from './ImageZoomViewerPage';
import LinearGradientPage from './LinearGradientPage';
// // import RenderHtmlPage from './src/RenderHtmlPage';
// // import SharePage from './src/SharePage';
// // import SkeletonPlaceholderPage from './src/SkeletonPlaceholderPage';
import WebViewPage from './WebViewPage';
// // import TooltipPage from './src/TooltipPage';
// // import VectorIconsPage from './src/VectorIconsPage';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="HomePage" component={HomePage} />
        <Stack.Screen name="NetinfoPage" component={NetinfoPage} />
        <Stack.Screen name="SliderPage" component={SliderPage} />
        <Stack.Screen name="GeolocationPage" component={GeolocationPage} />
        <Stack.Screen name="ClipboardPage" component={ClipboardPage} />
        <Stack.Screen name="AsyncStoragePage" component={AsyncStoragePage} />
        <Stack.Screen
          name="ImageZoomViewerPage"
          component={ImageZoomViewerPage}
        />
        <Stack.Screen
          name="LinearGradientPage"
          component={LinearGradientPage}
        />
        <Stack.Screen name="WebViewPage" component={WebViewPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
