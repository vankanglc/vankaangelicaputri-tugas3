/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Style,
  ScrollView,
} from 'react-native';

const HomePage = ({navigation}) => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F7F7F7',
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <ScrollView>
        <View
          style={{
            width: '100%',
            weight: 360,
            height: 640,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => navigation.navigate('NetinfoPage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Netinfo Page
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('SliderPage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Slider Page
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('GeolocationPage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Geolocation Page
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('ClipboardPage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Clipboard Page
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('AsyncStoragePage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Async Storage Page
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('WebViewPage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Image Zoom Viewer Page
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('WebViewPage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Linear Gradient Page
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('WebViewPage')}
            style={styles.touchable}>
            <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
              Web View Page
            </Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
          onPress={() => navigation.navigate('TooltipPage')}
          style={styles.touchable}>
          <Text style={{color: '#FFFFFF', textAlign: 'left'}}>
            Tooltip Page
          </Text>
        </TouchableOpacity> */}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: '#F4F4F4',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  touchable: {
    backgroundColor: '#2E3283',
    width: 280,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
});

export default HomePage;
