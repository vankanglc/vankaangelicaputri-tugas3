/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Routing from './src/Routing';

const App = () => {
  return <Routing />;
};

export default App;
